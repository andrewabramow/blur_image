#include "mainwindow.h"

#include <QApplication>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSlider>
#include <QFileDialog>
#include <QGraphicsScene>
#include <QGraphicsBlurEffect>
#include <QGraphicsPixmapItem>
#include <QPainter>

QImage blurImage(QImage source, int blurRadius)
{
    if (source.isNull()) return QImage();
    QGraphicsScene scene;
    QGraphicsPixmapItem item;
    item.setPixmap(QPixmap::fromImage(source));

    auto *blur = new QGraphicsBlurEffect;
    blur->setBlurRadius(blurRadius);
    item.setGraphicsEffect(blur);
    scene.addItem(&item);
    QImage result (source.size(), QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    QPainter painter (&result);
    scene.render(&painter, QRectF(),
                 QRectF(0,0,source.width(),source.height()));
    return result;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    // Gui
    QWidget imageWindow;
    QVBoxLayout vbox (&imageWindow);
    QHBoxLayout buttons (&imageWindow);
    //image
    auto imageWidget = new QLabel(&imageWindow);
    auto open = new QPushButton("Open", &imageWindow);
    auto save = new QPushButton("Save", &imageWindow);
    vbox.addWidget(imageWidget);
    buttons.addWidget(open);
    buttons.addWidget(save);
    vbox.addLayout(&buttons);
    QSlider *blurSlider = new QSlider(&imageWindow);
    blurSlider->setOrientation(Qt::Horizontal);
    blurSlider->setMinimum(0);
    blurSlider->setMaximum(10);
    vbox.addWidget(blurSlider);

    QString sourcePath;
    QString outputPath;

    QObject::connect(open, &QPushButton::clicked, [&sourcePath, &outputPath, imageWidget]
    {
        sourcePath = QFileDialog::getOpenFileName(nullptr,
                                                "Open image",
                                                "_paTh_",
                                                "image files (*.jpg , *.bmp , *.png)");
        QPixmap pix(sourcePath);
        imageWidget->setPixmap(pix);
    });

    int value = 0;

    QObject::connect(blurSlider, &QSlider::valueChanged, [&value,
                                                          &sourcePath,
                                                          blurSlider,
                                                          imageWidget]
    {

            value = blurSlider->value();
            imageWidget->setPixmap(QPixmap::fromImage(blurImage(QImage(sourcePath), value).scaled(
                                                imageWidget->width(),
                                                imageWidget->height(), Qt::KeepAspectRatio)));
    });

    QObject::connect(save, &QPushButton::clicked, [&sourcePath, &value]
    {
        auto outputPath = QFileDialog::getSaveFileName(nullptr,
                                                "Save image",
                                                "*name");

        auto blured = blurImage(QImage(sourcePath), value);
        blured.save(outputPath);
    });

    imageWindow.show();

    return a.exec();
}
